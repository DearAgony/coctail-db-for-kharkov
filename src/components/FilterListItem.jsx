import React from "react";


class FilterListItem extends React.Component{
    render(){
        return  <li className="item">
            <input type="checkbox" checked={this.props.status} onChange={(event) => this.props.onStatusChange(event.target.checked)} />
            {this.props.title}
            </li>
        ;
    }
}

export{
    FilterListItem
}