import React, { Fragment } from "react";



export class MainView extends React.PureComponent {
    render() {
        console.log("render", this.props.drinksByCategory);
        return <Fragment>
            {
                this.props.drinksByCategory ?
                    this.props.drinksByCategory.map(item =>
                        <Fragment key={item.category}>
                            <div className="main">
                                <br />

                                <div className="mainItems">
                                    <div className="main__category">
                                        <h3>{item.category}</h3>
                                        <hr />
                                    </div>
                                    {
                                        item.drinks.map(drink =>
                                            <div key={drink.idDrink} className="mainItem">
                                                <img className="drinkImage" src={drink.strDrinkThumb} alt="" />
                                                <a className="drinkName" href="">{drink.strDrink}</a>
                                            </div>)
                                    }
                                </div>
                            </div>
                        </Fragment>

                    ) 
                    : null
            }

        </Fragment>



    }
}
