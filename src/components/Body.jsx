import React, { Fragment } from "react";
import { Filters } from "./Filters";
import { MainView } from "./MainView";

export class Body extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filters: [],
            drinks: []
        }
        this.init();

    }
    init = async () => {
        let filters = await this.getFilters();
        this.setState({ filters: filters });
        this.showCategoryByFilters(filters);
    }
    getFilters = async () => {
        let responce = await fetch("https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list");
        let res = await responce.json();
        return res.drinks.map(item => item.strCategory);
    }
    showCategoryByFilters = async (filters) => {
        let res = [];
        for(let i=0; i<filters.length; i++) { 
            let drinksByCategory = await this.getDrinksByCategory(filters[i]);
            res.push(drinksByCategory);
        }
        console.log(res);
        this.setState({drinks: res});
    }
    getDrinksByCategory = async (category) => {
        let link = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=" + category;
        let encodedLink = encodeURI(link);
        let coctailsListResponse = fetch(encodedLink);
        let response = await coctailsListResponse;
        let value = await response.json();
        return {
            category: category,
            drinks: value.drinks
        };
    }
    render() {
        return <Fragment>
            <Filters onFiltersSelected={this.showCategoryByFilters} filters={this.state.filters} />
            <MainView drinksByCategory={this.state.drinks} />
        </Fragment>;
    }
}
