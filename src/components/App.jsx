
import React, { Fragment } from "react";
import { Body } from "./Body";
import { Header } from "./Header";

export const App = (props) => <Fragment>
        <Header/>
        <Body />
    </Fragment>