import React from "react";

export class Header extends React.PureComponent {
    render(){
        return <header>
        <div className="header">
            <div className="header__title">Coctail DB</div>
            <div className="logo">
                <a href="">
                    <img src="../images/logo.svg" alt="" />
                </a>
            </div>
        </div>
    </header>
    }
}