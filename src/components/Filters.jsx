import React, { Fragment } from "react";
import { Button } from "./Button";
import { FilterListItem } from "./FilterListItem";

export class Filters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filterStatuses: {}
        };

    }

    onFiltersStatusChange = (filterName, checked) => {
        let result = this.state.filterStatuses;
        result[filterName] = checked;
        this.setState({ filterStatuses: result });
    }

    onApply = () => {
        let res = [];

        for(let i=0; i<this.props.filters.length; i++){
            let filterName = this.props.filters[i];
            let filterStatus = this.state.filterStatuses[filterName];
            
            if(filterStatus == undefined || filterStatus){
                res.push(filterName);
            }
        }

        this.props.onFiltersSelected(res);
    }

    render() {
        return <Fragment>
            <div className="filterWrapper">
                <ul className="filter" >
                    {
                        this.props.filters ?
                            this.props.filters.map(filter => <FilterListItem
                                status={
                                    this.state.filterStatuses[filter] == undefined ?
                                        true 
                                        : this.state.filterStatuses[filter]
                                }
                                key={filter}
                                title={filter}
                                onStatusChange={(checked) => this.onFiltersStatusChange(filter, checked)}
                            />)
                            : null

                    }
                </ul>
                <Button title="Apply" onClick={this.onApply} />
            </div>
        </Fragment>
    }
}