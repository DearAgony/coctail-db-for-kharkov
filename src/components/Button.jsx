import React from "react";

const Button = (props) => <button className="applyButton" onClick={props.onClick}>{props.title}</button>;

export{
    Button
}